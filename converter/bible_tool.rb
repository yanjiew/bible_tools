require_relative "book_names"
require_relative "bible"

book_names = {'創世記' => 'GEN',
                       '出埃及記' => 'EXO',
                       '利未記' => 'LEV',
                       '民數記' => 'NUM',
                       '申命記' => 'DEU',
                       '約書亞記' => 'JOS',
                       '士師記' => 'JDG',
                       '路得記' => 'RUT',
                       '撒母耳記上' => '1SA',
                       '撒母耳記下' => '2SA',
                       '列王紀上' => '1KI',
                       '列王紀下' => '2KI',
                       '歷代志上' => '1CH',
                       '歷代志下' => '2CH',
                       '以斯拉記' => 'EZR',
                       '尼希米記' => 'NEH',
                       '以斯帖記' => 'EST',
                       '約伯記' => 'JOB',
                       '詩篇' => 'PSA',
                       '箴言' => 'PRO',
                       '傳道書' => 'ECC',
                       '雅歌' => 'SNG',
                       '以賽亞書' => 'ISA',
                       '耶利米書' => 'JER',
                       '耶利米哀歌' => 'LAM',
                       '以西結書' => 'EZK',
                       '但以理書' => 'DAN',
                       '何西阿書' => 'HOS',
                       '約珥書' => 'JOL',
                       '阿摩司書' => 'AMO',
                       '俄巴底亞書' => 'OBA',
                       '約拿書' => 'JON',
                       '彌迦書' => 'MIC',
                       '那鴻書' => 'NAM',
                       '哈巴谷書' => 'HAB',
                       '西番雅書' => 'ZEP',
                       '哈該書' => 'HAG',
                       '撒迦利亞書' => 'ZEC',
                       '瑪拉基書' => 'MAL',
                       '馬太福音' => 'MAT',
                       '馬可福音' => 'MRK',
                       '路加福音' => 'LUK',
                       '約翰福音' => 'JHN',
                       '使徒行傳' => 'ACT',
                       '羅馬人書' => 'ROM',
                       '哥林多前書' => '1CO',
                       '哥林多後書' => '2CO',
                       '加拉太書' => 'GAL',
                       '以弗所書' => 'EPH',
                       '腓立比書' => 'PHP',
                       '歌羅西書' => 'COL',
                       '帖撒羅尼迦前書' => '1TH',
                       '帖撒羅尼迦後書' => '2TH',
                       '提摩太前書' => '1TI',
                       '提摩太後書' => '2TI',
                       '提多書' => 'TIT',
                       '腓利門書' => 'PHM',
                       '希伯來書' => 'HEB',
                       '雅各書' => 'JAS',
                       '彼得前書' => '1PE',
                       '彼得後書' => '2PE',
                       '約翰一書' => '1JN',
                       '約翰二書' => '2JN',
                       '約翰三書' => '3JN',
                       '猶大書' => 'JUD',
                       '啟示錄' => 'REV'}

filename = ARGV.first
file = File.open(filename,'r')
dirname = ARGV[1]

# 正規表示法
# 書
book_title_reg = Regexp.new('^\"(?<book>.*?)\",\"(?<title>.*?)\",[0-9]+,[0-9]+$')

# 每章的第一個經文（用來判段章節名稱）
first_verse_reg = Regexp.new('^.+ (?<chapter>[0-9]+):1--')

# 一般的經文
verse_reg = Regexp.new('^.+ [0-9]+:(?<verse>[0-9]+)--(?<content>.*)$')

# 耶穌的話
jesus_word_reg = Regexp.new("'(?<word>.*?)'")

# Footnote
footnote_reg = Regexp.new('〔(?<footnote>.*?)〕|﹝(?<footnote>.*?)﹞')

# 段落標題
section_title_reg = Regexp.new('--【(?<title>.*?)】')

# 詩篇標題
psa_title_reg = Regexp.new('--｛(?<title>.*?)｝|--《(?<title>.*?)》')

# Speakers_at_beginning
speaker_reg = Regexp.new('--《(?<speaker>新郎|新婦|朋友)》')

# Speakers_at_middle
mspeaker_reg = Regexp.new('《(?<speaker>新郎|新婦|朋友)》')

par_reg = Regexp.new('（(?<content>.*[:：]+.*)）')
in_last_verse = Regexp.new('併入第.*節')

if ARGV[2]
  inc_file = File.open(ARGV[2],'r')
  eval inc_file.read, binding
  inc_file.close
end

bible=Bible.new
book=Book.new
chapter=Chapter.new

while line = file.gets
    paragraph=0
    if parts = line.match(book_title_reg)
      #新的一章
      chapter=Chapter.new
      abbr = book_names[parts['book']]
      chapter.title = parts['title']
      if bible.book_name(book) != abbr
        # new book
        book=Book.new
        name = BibleNames[abbr]
        book.name= name[1]
        book.abbr= name[2]
        bible.add(abbr,book)
      end
      next
    end
    
    if parts = line.match(first_verse_reg)
      # 找到章節標題，新的一章
      book.add(chapter,parts['chapter'].to_i)
      paragraph=1
    end

    # 找出標題
    if parts = line.match(section_title_reg)
      line.gsub!(section_title_reg,'--')
      section = parts['title']
      ref=nil
      if par= section.match(par_reg)
        section.gsub!(par_reg,'')
        ref=par['content']
        ref.gsub!('：',':')
      end
      chapter.add_stitle section
      if ref
        chapter.add_ref ref
      end
      paragraph=1
    end
    
    # 雅歌說話者
    if parts = line.match(speaker_reg)
      line.gsub!(speaker_reg,'--')
      speaker = parts['speaker']
      chapter.append_str "\\sp #{speaker}", true
      paragraph =1
    end

    # 詩篇標題
    if parts = line.match(psa_title_reg) 
      line.gsub!(psa_title_reg,'--')
      section = parts['title']
      chapter.add_psa_title section
      paragraph=1
    end
    if paragraph == 1 && bible.book_name(book) != 'PSA'
      chapter.add_paragraph
    end
    
    if ! parts = line.match(verse_reg)
      next
    end
    verse=Verse.new
    verse.content=parts['content']
    verse_num=parts['verse'].to_i
    verse.content.gsub!(verse_reg,'\\v \k<verse> ')
    verse.content.gsub!(jesus_word_reg,'\\wj \k<word>\\wj*')
    verse.content.gsub!(footnote_reg,'\\f + \k<footnote>\\f*')
    verse.content.gsub!(mspeaker_reg, "\n\\sp \\k<speaker>\n\\p\n")
    
    # 併入上節
    if verse.content.match(in_last_verse) || verse.content==''
      chapter.find(chapter.count).length+=1
      next
    end
    
    if bible.book_name(book) == 'PSA'
      chapter.append_str "\\q", true
    end
    chapter.add(verse,verse_num)
end
bible.output_usfm(dirname)