# Footnote
footnote_reg = Regexp.new('（(?<footnote>.*?)）')
# 段落標題
section_title_reg = Regexp.new('--【(?<title>.*?)】')
# 詩篇標題
psa_title_reg = Regexp.new('--（(?<title>.*?)）')
# Speakers_at_beginning_disabled
speaker_reg = Regexp.new('$(?<speaker>.)^')
# Speakers_at_middle_disabled
mspeaker_reg = Regexp.new('$(?<speaker>.)^')
par_reg = Regexp.new('（(?<content>.*[:：]+.*)）')
in_last_verse = Regexp.new('併入第.*節')