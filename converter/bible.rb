require 'fileutils'

class Bible
  attr_accessor :books
   def initialize
     @book = {}
   end
   
   def add(name,book)
     @book[name] = book
   end
   
   def remove(name)
     @book.delete(name)
   end

   def find(name)
     @book[name]
   end
   
   def book_name(book)
     @book.key(book)
   end

   def output_usfm(dir)
     FileUtils.mkdir_p dir
     @book.each do |name, book|
       file = File.open(dir + "/#{name}.usfm",'w')
       
       #標頭
       file.puts "\\id #{name}\n"
       file.puts "\\ide UTF-8\n"
       file.puts "\\h #{book.name}\n"
       if book.full_name
         file.puts "\\toc1 #{book.full_name}\n"
       end
       file.puts "\\toc2 #{book.name}\n"
       if book.abbr
         file.puts "\\toc3 #{book.abbr}\n"
       end
       if book.title == nil
         if book.full_name
           file.puts "\\mt #{book.full_name}\n"
         else
           file.puts "\\mt #{book.name}\n"
         end
       else
         file.puts "#{book.title}\n"
       end
       
       book.each do |ch_n, ch|
         file.puts "\n\\c #{ch_n.to_s}\n"
         if ch.title != nil
           file.puts "\\ms #{ch.title}\n"
         end
         ch.each do |v_n, v|
           content = v.content.gsub("<verse>", "#{ch_n.to_s}.#{v_n.to_s}")
           if v_n == 0
             file.puts "#{v.content}\n"  
           elsif v.length == 1
             file.puts "\\v #{v_n.to_s} #{content}\n"
           else
             file.puts "\\v #{v_n.to_s}-#{(v_n+v.length-1).to_s} #{content}\n"
           end
         end
       end

       file.close
     end
   end
end

class Book
  attr_accessor :full_name, :name, :abbr, :title, :chapters, :count
  def initialize
    @full_name = nil
    @name = ''
    @abbr = nil
    @title = nil
    @chapters = {}
    @count = 0
  end
  
  def add(chapter,n = -1)
    if n == -1
      @count=@count+1
      @chapters[@count] = chapter
    else
      @count=n
      @chapters[n] = chapter
    end
  end
  
  def remove(n)
    @chapters
  end
  
  def find(n)
    @chapters[n]
  end
  
  def each
    @chapters.keys.sort.each do |key|
      yield key, @chapters[key]
    end
  end
  
  def chapter_num(chapter)
    @chapters.key(chapter)
  end
end

class Chapter
  attr_accessor :verses, :count, :title
   def initialize
	   @verses = {}
	   @count = 0
     @verses[0] = Verse.new
   end
   
   def add(verse,n = -1)
     if verse.length <= 0 || n == 0
	     verse.length=1
	   end
     if n == -1
       @count+=@verses[@count].length
       @verse[@count] = verse
     else
       @count=n
	     @verses[n] = verse
     end
   end
   
   def append_str(str,anl=false,n=-1)
     if n == -1
       @verses[@count].append str, anl
     else
       @verses[n].append str, anl
     end
   end
   
   def add_paragraph(n = -1)
     append_str "\\p", true
   end
   
   def add_stitle(stitle,n = -1)
     append_str "\\s #{stitle}", true
   end

   def add_psa_title(psa_title,n = -1)
     append_str "\\d #{psa_title}", true
   end
   
   def add_ref(ref,n=-1)
     append_str "\\r #{ref}", true
   end
   
   def find(n)
     @verses[n]
   end
   
   def verse_num(verse)
     @verses.key(verse)
   end
   
   def each
     @verses.keys.sort.each do |key|
       yield key, @verses[key]
     end
   end
   
   def remove(n)
     @verses
   end
end

class Verse
   attr_accessor :length, :content
   def initialize
     @length = 1
	   @content = ''
   end
   
   def append(c, anl=false)
     if anl && content != ''
       @content = @content + "\n"
     end
     @content = @content + c
   end

end