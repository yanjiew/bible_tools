聖經轉換工具，用於轉成 USFM 格式

bible_tool.rb -- 用於耶大雅聖經工具中的聖經原始檔（使用非和合本，需外掛檔才能正確轉換）。
  Usage: bible_tool.rb filename.txt output_dir [additions]
wiki_bible.rb -- 用於轉換維基文庫中的和合本聖經（文理和合本不適用）。 可成功轉換注釋、標注的外加字、Cross Reference
  Usage: wiki_bible.rb filename.txt output_dir
bible.rb -- 聖經相關的 classes
utils.rb -- 一些小工具
book_names.rb -- 聖經書名對照表

+bible_tool_additions 耶大雅聖經工具中的聖經轉換工具外掛檔
 | CLV.rb 當代中譯本
 | NCV.rb 新譯本
 | LZZ.rb 呂振中譯本