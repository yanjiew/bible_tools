require_relative "book_names"
require_relative "bible"
require_relative "utils"

book_names = {'創世記' => 'GEN',
                       '出埃及記' => 'EXO',
                       '利未記' => 'LEV',
                       '民數記' => 'NUM',
                       '申命記' => 'DEU',
                       '約書亞記' => 'JOS',
                       '士師記' => 'JDG',
                       '路得記' => 'RUT',
                       '撒母耳記上' => '1SA',
                       '撒母耳記下' => '2SA',
                       '列王紀上' => '1KI',
                       '列王紀下' => '2KI',
                       '歷代志上' => '1CH',
                       '歷代志下' => '2CH',
                       '以斯拉記' => 'EZR',
                       '尼希米記' => 'NEH',
                       '以斯帖記' => 'EST',
                       '約伯記' => 'JOB',
                       '詩篇' => 'PSA',
                       '箴言' => 'PRO',
                       '傳道書' => 'ECC',
                       '雅歌' => 'SNG',
                       '以賽亞書' => 'ISA',
                       '耶利米書' => 'JER',
                       '耶利米哀歌' => 'LAM',
                       '以西結書' => 'EZK',
                       '但以理書' => 'DAN',
                       '何西阿書' => 'HOS',
                       '約珥書' => 'JOL',
                       '阿摩司書' => 'AMO',
                       '俄巴底亞書' => 'OBA',
                       '約拿書' => 'JON',
                       '彌迦書' => 'MIC',
                       '那鴻書' => 'NAM',
                       '哈巴谷書' => 'HAB',
                       '西番雅書' => 'ZEP',
                       '哈該書' => 'HAG',
                       '撒迦利亞書' => 'ZEC',
                       '瑪拉基書' => 'MAL',
                       '馬太福音' => 'MAT',
                       '馬可福音' => 'MRK',
                       '路加福音' => 'LUK',
                       '約翰福音' => 'JHN',
                       '使徒行傳' => 'ACT',
                       '保羅達羅馬人書' => 'ROM',
                       '保羅達哥林多人前書' => '1CO',
                       '保羅達哥林多人後書' => '2CO',
                       '保羅達加拉太人書' => 'GAL',
                       '保羅達以弗所人書' => 'EPH',
                       '保羅達腓立比人書' => 'PHP',
                       '保羅達歌羅西人書' => 'COL',
                       '保羅達帖撒羅尼迦人前書' => '1TH',
                       '保羅達帖撒羅尼迦人後書' => '2TH',
                       '保羅達提摩太前書' => '1TI',
                       '保羅達提摩太後書' => '2TI',
                       '保羅達提多書' => 'TIT',
                       '保羅達腓利門書' => 'PHM',
                       '希伯來書' => 'HEB',
                       '雅各書' => 'JAS',
                       '彼得前書' => '1PE',
                       '彼得後書' => '2PE',
                       '約翰一書' => '1JN',
                       '約翰二書' => '2JN',
                       '約翰三書' => '3JN',
                       '猶大書' => 'JUD',
                       '啟示錄' => 'REV'}
                       


filename = ARGV.first
file = File.open(filename,"r")
dirname = ARGV[1]

book_title_reg = Regexp.new('^\| section  = (?<book>.*)$')
chapter_reg = Regexp.new('{{Chapter\|(?<chapter>[0-9]+)\|.*}}')
verse_reg = Regexp.new('{{verse\|(?<chapter>[0-9]+)\|(?<verse>[0-9]+)}}')
verse_reg2 = Regexp.new('{{verse\|(?<chapter1>[0-9]+)\|(?<verse1>[0-9]+)}}.*{{verse\|(?<chapter2>[0-9]+)\|(?<verse2>[0-9]+)}}')
footnote_reg = Regexp.new('{{\*\|(?<footnote>.*?)}}')
chinese_word_note = Regexp.new('{{!\|(?<word>.*?)\|(?<note>.*?)}}');
udots_reg = Regexp.new('{{udots\|(?<udots>.*?)}}')
cross_reference_reg = Regexp.new('<sup>\[\[#(?<id>[0-9a-zA-Z]+?)\|.+?\]\]</sup>')
cross_reference_header_reg = Regexp.new(':<span id="(?<id>[0-9a-zA-Z]+?)" /><sup>.*?</sup>|:<span id="(?<id>[0-9a-zA-Z]+?)" \\\\><sup>.*?</sup>')
cross_reference_content_reg =  Regexp.new('\[\[../(?<book>.+?)#(?<chapter>[0-9]+?):(?<verse>[0-9]+?)\|.*?\]\]')
sub_reg = Regexp.new('<sup>.*?</sup>')
psa_title_reg = Regexp.new('{{\-\|(?<title>.*?)}}')
selah_reg = Regexp.new('{{\-\|細拉}}')

bible=Bible.new
book=Book.new
chapter=Chapter.new
cross_reference={}

while line = file.gets
    if parts = line.match(book_title_reg)
      abbr = book_names[parts['book']]
      if bible.book_name(book) != abbr
        # new book
        book=Book.new
        name = BibleNames[abbr]
        book.full_name = parts['book']
        book.name= name[1]
        book.abbr= name[2]
        bible.add(abbr,book)
      end
      next
    end
    
    #新的一章
    if parts = line.match(chapter_reg)

      # 處理 cross reference
      pos_offset=0
      last_verse=nil
      cross_reference.each do |k,v|
        until last_verse == v[:verse]
          pos_offset=0
          last_verse= v[:verse]
        end
        xt = "\\x + \\xo #{v[:xo]}: \\xt OT: " + v[:xt].join('; ') + " \\x*"
        v[:verse].content.insert(v[:position]+pos_offset,xt)
        pos_offset+=xt.length
      end 

      chapter=Chapter.new
      cross_reference={}
      
      if bible.book_name(book) != 'PSA' 
        chapter.add_paragraph
      end
      
      book.add(chapter,parts['chapter'].to_i)
    end
    
    if parts = line.match(verse_reg)
      verse=Verse.new
      n=parts['verse'].to_i
      if p=line.match(verse_reg2)
        verse.length=p['verse2'].to_i-n+1
      end

      line.gsub!(chinese_word_note, '\k<word>')
      line.gsub!(verse_reg2,'')
      line.gsub!(verse_reg,'')
      line.gsub!(udots_reg,'\add \k<udots>\add*')
      line.gsub!(footnote_reg,"\\f + \\fr <verse>: \\ft \\k<footnote>\\f*")
      line.gsub!(selah_reg,"\\qs 細拉\\qs*")
      
      if p = line.match(psa_title_reg)
        line.gsub!(psa_title_reg,'')
        chapter.add_psa_title p['title']
      end
      
      m = matches(line, cross_reference_reg)
      line.gsub!(cross_reference_reg, '')
      
      pos_offset=0
      m.each do |item|
        # we don't know the content
        cross_reference[item['id']] = {
          :xo => "<verse>",
          :verse => verse,
          :xt => [],
          :position => item.begin(0)+pos_offset
        }
        pos_offset-=item.offset(0)[1]-item.offset(0)[0]
      end
      

      line.chomp!
      verse.content=line
      if bible.book_name(book) == 'PSA'
        chapter.append_str "\\q", true
      end
      chapter.add(verse,n)
    end
    
    if parts = line.match(cross_reference_header_reg)
      id = parts['id']
      line.gsub!(cross_reference_header_reg,'')
      m = matches(line, cross_reference_content_reg)
      m.each do |item|
        bible_name = BibleNames.detect do |k,v|
          v[1] == item['book']
        end
        cross_reference[id][:xt].push "#{bible_name[1][2]} #{item['chapter']}.#{item['verse']}"
      end
    end
end
bible.output_usfm(dirname)